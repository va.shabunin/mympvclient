const net = require("net");
const EE = require("events");

const MpvClient = params => {
  let _params = {
    socketFile: `${process.env["XDG_RUNTIME_DIR"]}/mympv.sock`,
    debug: true
  };
  Object.assign(_params, params);

  // init self object as EE
  let self = new EE();
  let _socket;
  let _socketFile = _params.socketFile;
  let _debug = _params.debug;

  // store for requets. {id: num, ..payload..}
  // used for promise resolving/rejecting
  let _requests = [];

  self.debug = (...args) => {
    if (_debug) {
      console.log(`MpvClient:: ${args.map(t => t.toString())}`);
    }
  };
  self.connect = _ => {
    // try to connect to unix socket
    // if good then resolve Promise, bad - reject
    return new Promise((resolve, reject) => {
      self.debug(`connecting to ${_socketFile}`);
      _socket = net.createConnection(_socketFile);
      _socket.on("connect", _ => {
        _socket.on("data", data => {
          try {
            data
              .toString()
              .split("\n")
              .forEach(l => {
                if (l.length > 0) {
                  self.debug(`incoming data: ${l}`);
                  let payload = JSON.parse(l);

                  // if we deal with request
                  let findById = t => {
                    return t.id === payload.request_id;
                  };
                  let _request = _requests.find(findById);
                  if (_request) {
                    if (payload.error === "success") {
                      _request.resolve(payload);
                    } else {
                      _request.reject(payload);
                    }
                  }
                  // now events
                  if (typeof payload.event !== "undefined") {
                    self.emit("event", payload);
                  }
                }
              });
          } catch (e) {
            self.emit("error", e);
          }
        });
        resolve(_socket);
      });
      _socket.once("error", reject);
      // TODO: register listeners
      // on data, on close
    });
  };

  self.request = payload => {
    return new Promise((resolve, reject) => {
      try {
        let _id = Math.floor(Math.random() * 0xffff);
        let _request = { request_id: _id };
        Object.assign(_request, payload);
        self.debug("sending request", JSON.stringify(_request));
        _socket.write(`${JSON.stringify(_request)}\n`);
        // if writing was successful then store request to
        _requests.push({ id: _id, resolve: resolve, reject: reject });
      } catch (e) {
        reject(e);
      }
    });
  };

  self.loadfile = (filename, mode = "replace") => {
    return self.request({ command: ["loadfile", filename, mode] });
  };

  self.stop = _ => {
    return self.request({ command: ["stop"] });
  };

  self.pause = (state = true) => {
    return self.setProperty("pause", state);
  };

  self.volume = (volume = 100) => {
    return self.setProperty("volume", volume);
  };

  // set/get property
  self.getProperty = property => {
    return self.request({ command: ["get_property", property] });
  };
  self.setProperty = (property, value) => {
    return self.request({ command: ["set_property", property, value] });
  };

  return self;
};

module.exports = MpvClient;
