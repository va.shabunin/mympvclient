# mympvclient client to mpv ipc

## Usage

```js
const MyMpvClient = require('mympvclient');

let myClient = MyMpvClient({debug: true});

myClient.on('error', e => {
  console.log(e.message);
});

myClient.on('event', console.log);

file = 'https://www.youtube.com/watch?v=q43G9FTaomg';

const init = async _ => {
  try {
    await myClient.connect();
    console.log('connected, sending request');
    await myClient.stop();
    await myClient.loadfile(file);
    await myClient.volume(100);
  } catch (e) {
    console.log('aaargh', e.message);
  }
};

init();
```

## API

### `loadfile(filename, mode = 'replace')`

### `stop()`

### `pause(state = true)`

### `volume(value = 100)`

### `getProperty(property)`

### `setProperty(property, value)`

## Events

`myClient.on('events', console.log);`
