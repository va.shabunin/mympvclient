const MyMpvClient = require("./index.js");
const fs = require("fs");

let myClient = MyMpvClient({ debug: true });

myClient.on("error", e => {
  console.log(e.message);
});

myClient.on("event", console.log);

let playlist = [];

let file1;
file1 = "https://www.youtube.com/watch?v=gmHrHQEmp2Q"; // pink floyd
file1 = "https://www.youtube.com/watch?v=KXHCrwKU4Qc"; // casualties of cool

let index = 0;
playlist.push(file1);

const init = async _ => {
  try {
    await myClient.connect();
    console.log("connected, sending request");
    await myClient.stop();
    await myClient.loadfile(playlist[index]);
    await myClient.volume(42);
  } catch (e) {
    console.log("aaargh", e.message);
  }
};

init();
